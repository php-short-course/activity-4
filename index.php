<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC Activity 4</title>
</head>
<body>

	<h3>Building</h3>
	<?= $building->printName(); ?><br>
    <?= $building->printFloors(); ?><br>
    <?= $building->printAddress(); ?><br>
    <?php $building->setName('Caswynn Complex'); ?>
    <?= $building->printChange(); ?>

	<h3>Condominium</h3>
    <?= $condominium->printName(); ?><br>
    <?= $condominium->printFloors(); ?><br>
    <?= $condominium->printAddress(); ?><br>
    <?php $condominium->setName('Enzo Tower'); ?>
    <?= $condominium->printChange(); ?>
</body>
</html>
