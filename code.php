<?php

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function printName(){
		return "The name of this building is $this->name";
	}

     public function printFloors(){
		return "The $this->name has $this->floors floors";
	}

    public function printAddress(){
		return "The $this->name is located at $this->address";
	}

    public function printChange(){
		return "The name of this building has been changed to $this->name";
	}
}

class Condominium extends Building {
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function printName(){
		return "The name of this condominium is $this->name";
	}

    public function printFloors(){
		return "The $this->name has $this->floors floors";
	}

    public function printAddress(){
		return "The $this->name is located at $this->address";
	}

    public function printChange(){
		return "The name of this condominium has been changed to $this->name";
	}
}

$building = new Building ('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium ('Enzo Building', 5, 'Buendia Avenue, Makati City, Philippines');